const d = require('../data/data');
const y = require('../root/problem5');

const testData = [
    'Accord', 'Galant', 'riolet', '4000CS Quattro', 
    'Windstar', 'Cavalier','Ram Van 1500', 'Skylark', 
    'Prizm', 'Bravada', 'Familia', 'Wrangler', 'Talon', 
    'Topaz', 'GTO', 'Ram Van 3500', 'Escort', 'LSS', 'Camry', 
    'Econoline E250', 'Mustang', 'Yukon','Ciera', 'Sebring', 'Town Car'
]

let data = d.data
let carArr = y.getYearData(data)

if(JSON.stringify(carArr) === JSON.stringify(testData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!')
}