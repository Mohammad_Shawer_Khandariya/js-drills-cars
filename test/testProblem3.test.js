const d = require('../data/data');
const y = require('../root/problem3');

const testData = [
    '300M',           '4000CS Quattro',       '525',
    '6 Series',       'Accord',               'Aerio',
    'Bravada',        'Camry',                'Cavalier',
    'Ciera',          'Defender Ice Edition', 'E-Class',
    'Econoline E250', 'Escalade',             'Escort',
    'Esprit',         'Evora',                'Express 1500',
    'Familia',        'Fortwo',               'G35',
    'GTO',            'Galant',               'Intrepid',
    'Jetta',          'LSS',                  'MR2',
    'Magnum',         'Miata MX-5',           'Montero Sport',
    'Mustang',        'Navigator',            'Prizm',
    'Q',              'Q7',                   'R-Class',
    'Ram Van 1500',   'Ram Van 3500',         'Sebring',
    'Skylark',        'TT',                   'Talon',
    'Topaz',          'Town Car',             'Windstar',
    'Wrangler',       'Wrangler',             'XC70',
    'Yukon',          'riolet'
]
const data = d.data
const returnedData = y.dataSort(data)

if (JSON.stringify(returnedData) === JSON.stringify(testData)) {
    console.log('Test Passed!');
} else {
    console.log('Test Failed!')
}