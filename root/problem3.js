// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const x = require('../data/data')

let y = x.data

function dataSort(data) {
    let arrData = [];
    let i, j, temp;

    for(k = 0; k < data.length; k++) {
        arrData.push(data[k].car_model);
    }

    for (i = 0; i < arrData.length; i++) {
        j = i + 1;
        for (j = i; j < arrData.length; j++) {
            if (arrData[j] < arrData[i]) {
                temp = arrData[i];
                arrData[i] = arrData[j];
                arrData[j] = temp;
            }
        }
    }
    return(arrData);
}

console.log(dataSort(y));

module.exports = {
    dataSort
}