const x = require('../data/data')

let data = x.data

function bmwAudi(data) {
    let BMWAndAudi = [];
    for(i = 0; i < data.length; i++) {
        if (data[i].car_make === 'BMW' || data[i].car_make === 'Audi') {
            BMWAndAudi.push(data[i]);
        }
    }
    return(BMWAndAudi)
}

const cars = bmwAudi(data);

console.log(JSON.stringify(cars));

module.exports = {
    bmwAudi
}