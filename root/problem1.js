// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const x = require('../data/data');

let getData = x.data;

function findCarDetails(id) {
    for (i = 0; i < getData.length; i++) {
        if (getData[i].id === id) {
            return(`Car ${id} is a ${getData[i].car_year} ${getData[i].car_make} ${getData[i].car_model}`);
        }
    }
}

console.log(findCarDetails(33));

module.exports = {
    findCarDetails
};