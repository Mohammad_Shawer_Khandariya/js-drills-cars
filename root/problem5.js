// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const x = require('../data/data')

const z = require('./problem4')

let data = x.data

let modelYear = z.findYear(data);

function getYearData(year) {
    let car = [];
    for(let i = 0; i < modelYear.length; i++) {
        if (modelYear[i] < 2000) {
            car.push(data[i].car_model);
        }
    }
    return(car);
}

const carBefore2000 = getYearData(modelYear);
console.log(carBefore2000);

module.exports = {
    getYearData
}