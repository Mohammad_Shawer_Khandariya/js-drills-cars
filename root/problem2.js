// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the y is?  Log the make and model into the console in the format of: 
// "Last car is a *car make goes here* *car model goes here*"

const x = require('../data/data')

let y = x.data

function lastCar() {
    last = y.length - 1
    return(`Last car is a ${y[last].car_make} ${y[last].car_model}`)
}

console.log(lastCar());

module.exports = {
    lastCar
}